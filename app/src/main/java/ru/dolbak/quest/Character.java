package ru.dolbak.quest;

public class Character {
    private int skill;
    private int tiredness;
    private int money;
    private int reputation;

    public Character(int skill, int tiredness, int money, int reputation) {
        this.skill = skill;
        this.tiredness = tiredness;
        this.money = money;
        this.reputation = reputation;
    }
}

class Task{
    private int id;
    private String text;
    private Action action1, action2, action3;

    public Task(int id, String text, Action action1, Action action2, Action action3) {
        this.id = id;
        this.text = text;
        this.action1 = action1;
        this.action2 = action2;
        this.action3 = action3;
    }
}

class Action{
    private String text;
    private int skill;
    private int tiredness;
    private int money;
    private int reputation;
    private int nextTask;

    public Action(String text, int skill, int tiredness, int money, int reputation, int nextTask) {
        this.text = text;
        this.skill = skill;
        this.tiredness = tiredness;
        this.money = money;
        this.reputation = reputation;
        this.nextTask = nextTask;
    }
}

class Preparator{
    Task[] makeTaks(){
        Task[] tasks = new Task[1];
        Task task1 = new Task(1, "Вам предлагают взломать сайт ООО \"Рога и Копыта\". Ваши действия: ",
                new Action("Честно выполнить задание", 50, 90, 1000, 100, 2),
                new Action("Лениново сделать задание", 10, 40, 500, 50, 2),
                new Action("Взять деньги и кинуть", 0, 0, 1000, -40, 2));
        tasks[0] = task1;
        return tasks;
    }
}
